
#include "Game.h"

using namespace sf;
using namespace std;




/*
Prototype game, this version focuses on collision detection
*/
int main()
{
	// Create the main window
	RenderWindow window(VideoMode(GC::SCREEN_RES.x, GC::SCREEN_RES.y), "Rock Dodger");

	Texture texShip;
	LoadTexture("data/ship.png", texShip);

	Texture texRock;
	LoadTexture("data/asteroid.png", texRock);

	vector<Object> objects;
	Object ship;
	ship.Init(window, texShip, Object::ObjectT::Ship);
	objects.push_back(ship);

	PlaceRocks(window, texRock, objects);

	Clock clock;

	// Start the game loop 
	while (window.isOpen())
	{
		// Process events
		Event event;
		while (window.pollEvent(event))
		{
			// Close window: exit
			if (event.type == Event::Closed) 
				window.close();
			if (event.type == Event::TextEntered)
			{
				if (event.text.unicode == GC::ESCAPE_KEY)
					window.close(); 
			}
		} 

		// Clear screen
		window.clear();

		float elapsed = clock.getElapsedTime().asSeconds();
		clock.restart();
		
		CheckCollisions(objects, window);
		for (size_t i = 0; i < objects.size(); ++i)
		{
			objects[i].Update(window, elapsed);
			objects[i].Render(window, elapsed);
		}


		// Update the window
		window.display();
	}

	return EXIT_SUCCESS;
}
