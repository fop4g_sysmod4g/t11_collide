#pragma once

#include "SFML/Graphics.hpp"

//dimensions in 2D that are whole numbers
struct Dim2Di
{
	int x, y;
};

//dimensions in 2D that are floating point numbers
struct Dim2Df
{
	float x, y;
};


/*
A box to put Games Constants in.
These are special numbers with important meanings (screen width,
ascii code for the escape key, number of lives a player starts with,
the name of the title screen music track, etc.
*/
namespace GC
{
	//game play related constants to tweak
	const Dim2Di SCREEN_RES{ 800,600 };
	const float SPEED = 250.f;			//ship speed
	const float SCREEN_EDGE = 0.6f;		//how close to the edge the ship can get
	const char ESCAPE_KEY{ 27 };
	const float ROCK_MIN_DIST = 2.15f;	//used when placing rocks to stop them getting too close
	const int NUM_ROCKS = 30;			//how many to place
	const int PLACE_TRIES = 10;			//how many times to try and place before giving up
}

/*
A game object that could be a rock or the player
Objects are anything with a sprite that can move around the screen
and collide with other objets.
*/
struct Object
{
	sf::Sprite spr;	//main image
	float radius;	//collision radius
	enum class ObjectT { Ship, Rock };	//what is this?
	ObjectT type;
	bool colliding = false; //did we hit something on the last update

	/*
	Call this to setup your object
	window - sfml render window
	tex - texture to use on the sprite
	type - what is it meant to be
	*/
	void Init(sf::RenderWindow& window, sf::Texture& tex, ObjectT type_);
	//called by Init as needed
	void InitShip(sf::RenderWindow& window, sf::Texture& tex);
	//called by Init as needed
	void InitRock(sf::RenderWindow& window, sf::Texture& tex);
	//move and update logic
	void Update(sf::RenderWindow& window, float elapsed);
	//draw
	void Render(sf::RenderWindow& window, float elapsed);
	//handle moving the ship around
	void PlayerControl(sf::Vector2u& screenSz, float elapsed);
};

/*
Update every object to see if it is colliding with any other - sets the colliding flag true
objects - any could be colliding
debug - if true, draw the collision radius and mark any collisions in red
*/
void CheckCollisions(std::vector<Object>& objects, sf::RenderWindow& window, bool debug = true);
//
void DrawCircle(sf::RenderWindow& window, const sf::Vector2f& pos, float radius, sf::Color col);
/*
file - path and file name and extension
tex - set this up with the texture
*/
bool LoadTexture(const std::string& file, sf::Texture& tex);
/*
Check if two circles are touching
pos1,pos2 - two centres
minDist - minimum colliding distance
*/
bool CircleToCircle(const sf::Vector2f& pos1, const sf::Vector2f& pos2, float minDist);
/*
Test one object against an array of other objects to see if it collides
It's OK if the object happens to be in the array, it won't test against itself
*/
bool IsColliding(Object& obj, std::vector<Object>& objects);
/*
Place all the rocks at the start of the game so they are not touching and maintain
a safe distance between them so the player can fit through
NOTE - it uses random placement and isn't guaranteed to place all the rocks
tex - needs the rock texture as it will call init on each one it places
*/
void PlaceRocks(sf::RenderWindow& window, sf::Texture& tex, std::vector<Object>& objects);

