#include <assert.h>
#include <string>
#include <math.h>

#include "Game.h"

using namespace sf;
using namespace std;


void Object::InitShip(RenderWindow& window, Texture& tex)
{
	spr.setTexture(tex, true);
	const IntRect& texRect = spr.getTextureRect();
	spr.setOrigin(texRect.width / 2.f, texRect.height / 2.f);
	spr.setScale(0.1f, 0.1f);
	spr.setRotation(90);
	spr.setPosition(spr.getGlobalBounds().width*0.6f, window.getSize().y / 2.f);
	type = ObjectT::Ship;
	radius = 25.f;
}

void Object::InitRock(RenderWindow& window, Texture& tex)
{
	spr.setTexture(tex);
	IntRect texR(0, 0, 96, 96);
	spr.setTextureRect(texR);
	spr.setOrigin(texR.width / 2.f, texR.height / 2.f);
	spr.setPosition(window.getSize().x*0.5f, window.getSize().y * 0.5f);
	radius = 10.f + (float)(rand() % 30);
	float scale = 0.75f * (radius / 25.f);
	spr.setScale(scale, scale);
}

void Object::Init(RenderWindow& window, Texture& tex, ObjectT type_)
{
	switch (type_)
	{
	case ObjectT::Ship:
		InitShip(window, tex);
		break;
	case ObjectT::Rock:
		InitRock(window, tex);
		break;
	default:
		assert(false);
	}
}

void Object::Update(RenderWindow& window, float elapsed)
{
	colliding = false;
	switch (type)
	{
	case ObjectT::Ship:
		PlayerControl(window.getSize(), elapsed);
		break;
	}
}

void Object::Render(RenderWindow& window, float elapsed)
{
	window.draw(spr);
}

void Object::PlayerControl(Vector2u& screenSz, float elapsed)
{
	Vector2f pos = spr.getPosition();
	FloatRect rect = spr.getGlobalBounds();
	if (Keyboard::isKeyPressed(Keyboard::Up))
	{
		if (pos.y > (rect.height*GC::SCREEN_EDGE))
			pos.y -= GC::SPEED * elapsed;
	}
	else if (Keyboard::isKeyPressed(Keyboard::Down))
	{
		if (pos.y < (screenSz.y - rect.height*GC::SCREEN_EDGE))
			pos.y += GC::SPEED * elapsed;
	}

	if (Keyboard::isKeyPressed(Keyboard::Left))
	{
		if (pos.x > (rect.width*GC::SCREEN_EDGE))
			pos.x -= GC::SPEED * elapsed;
	}
	else if (Keyboard::isKeyPressed(Keyboard::Right))
	{
		if (pos.x < (screenSz.x - rect.width*GC::SCREEN_EDGE))
			pos.x += GC::SPEED * elapsed;
	}

	spr.setPosition(pos);
}

bool LoadTexture(const string& file, Texture& tex)
{
	if (tex.loadFromFile(file))
	{
		tex.setSmooth(true);
		return true;
	}
	assert(false);
	return false;
}


void DrawCircle(RenderWindow& window, const Vector2f& pos, float radius, Color col)
{
	CircleShape c;
	c.setRadius(radius);
	c.setPointCount(20);
	c.setOutlineColor(col);
	c.setOutlineThickness(2);
	c.setFillColor(Color::Transparent);
	c.setPosition(pos);
	c.setOrigin(radius, radius);
	window.draw(c);
}

bool CircleToCircle(const Vector2f& pos1, const Vector2f& pos2, float minDist)
{
	float dist = (pos1.x - pos2.x) * (pos1.x - pos2.x) +
		(pos1.y - pos2.y) * (pos1.y - pos2.y);
	dist = sqrtf(dist);
	return dist <= minDist;
}

void CheckCollisions(vector<Object>& objects, RenderWindow& window, bool debug)
{
	if (objects.size() > 1)
	{
		for (size_t i = 0; i < objects.size(); ++i)
		{
			Object& a = objects[i];
			if (i < (objects.size() - 1))
				for (size_t ii = i + 1; ii < (objects.size()); ++ii)
				{
					Object& b = objects[ii];
					if (CircleToCircle(a.spr.getPosition(), b.spr.getPosition(), a.radius + b.radius))
					{
						a.colliding = true;
						b.colliding = true;
					}
				}
			if (debug)
			{
				Color col = Color::Green;
				if (a.colliding)
					col = Color::Red;
				DrawCircle(window, a.spr.getPosition(), a.radius, col);
			}
		}
	}
}


bool IsColliding(Object& obj, vector<Object>& objects)
{
	size_t idx = 0;
	bool colliding = false;
	while (idx < objects.size() && !colliding) {

		if (&obj != &objects[idx])
		{
			const Vector2f& posA = obj.spr.getPosition();
			const Vector2f& posB = objects[idx].spr.getPosition();
			float dist = obj.radius + objects[idx].radius;
			colliding = CircleToCircle(posA, posB, dist);
		}
		++idx;
	}
	return colliding;
}


void PlaceRocks(RenderWindow& window, Texture& tex, vector<Object>& objects)
{
	bool space = true;
	int ctr = GC::NUM_ROCKS;
	while (space && ctr)
	{
		Object rock;
		rock.Init(window, tex, Object::ObjectT::Rock);
		rock.radius *= GC::ROCK_MIN_DIST;
		int tries = 0;
		do {
			tries++;
			float x = (float)(rand() % window.getSize().x);
			float y = (float)(rand() % window.getSize().y);
			rock.spr.setPosition(x, y);
		} while (tries < GC::PLACE_TRIES && IsColliding(rock, objects));
		rock.radius *= 1 / GC::ROCK_MIN_DIST;
		if (tries != GC::PLACE_TRIES)
			objects.push_back(rock);
		else
			space = false;
		--ctr;
	}
}
